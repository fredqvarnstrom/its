# -*- coding: utf-8 -*-
"""common config for ITS.
"""
import platform

# define main roads length, meter.
length_road_left = length_road_right = 60

# define main roads width, meter.
width_road = 30

# define side street (side street) length, meter.
length_side = 60

# define side street (side street) width, meter.
width_side = 18

# define workzones length, meter.
length_workzones = 30

# define workzones width, meter.
width_workzones = 15

# define extended limit box for workzones, meter. it's width is same as main roads width.
length_limitzones = 80

# define count of side roads.
count_sideroads = 3

# define stop offset at the end of road, meter.
# e.g. when vehicle arrives at the location that far away from end of road less than the offset
# system will detect the vehicle arrived the end of the road.
stop_offset = 8

# define drawing client size, pixels.
client_width = 1600
client_height = 1200

# define average length of vehicles (include gap between vehicles), meter.
length_vehicle = 4

# define average speed of vehicle, meter / second
speed_vehicle = 4

# define time interval that update vehicle's location, second
update_interval = 1

# define a fixed minimum waiting time for vehicles in a queue to enter into extended limit box for workzones, second.
minimum_wait_time = 0

# define a min decision interval, second
minimum_decision_interval = 3

# define how many states to be used in DP solving, e.g. how many sequence we want to schedule.
num_states = 4

# define how many seconds for yellow signal to last. As discussed, yellow signal will last fixed seconds.
yellow_duration = 5

# define how many seconds for turn yellow signal light from green if no vehicle arrival in this duration.
green_wait_max = 10

# define how many seconds for keep green to wait coming vehicle to arrive.
green_wait_min = 5

# define minimum duration once green light
minimum_green_duration = 5

# Port name of XBee adapter
if platform.system() == 'Windows':
    xbee_port = 'COM5'
else:
    xbee_port = '/dev/ttyUSB0'


# Baud rate of XBee tunnel
xbee_baud = 9600

# xbee_master_addr = "0013a20040db7ba6"
xbee_master_addr = "0013a20040d91ab1"

xbee_node_dir_0_addr = '0013a20040d91ab0'
xbee_node_dir_1_addr = '0013a20040d91aaf'








