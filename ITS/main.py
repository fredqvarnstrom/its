# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

from time import sleep
import threading

from models.roads import RoadMain, RoadSide, Limitzones
from models.signals import Signal, States

from models.direction import Direction
from models.location import Location

from algorithm.dp import DP
from algorithm.arla import ARla
from algorithm.arloc import ARloc

from receiver.vehicle_receiver import VehicleReceiver

import config


class ITSManage:

    vehicles = None
    roads = None
    limitzones = None
    signals = None
    cx = None
    cy = None

    def __init__(self):
        pass

    def initialize(self):
        """
        Generate roads, signals, states, etc.
        """

        # generate roads.
        self.roads = [
            RoadMain("road_LR", config.length_road_left, Direction.LR),
            RoadMain("road_RL", config.length_road_right, Direction.RL),
        ] + [RoadSide("sideway_%d" % (i+1), i, config.length_side, Direction.TB) for i in range(config.count_sideroads)]

        # generate limit zones and work zones.
        self.limitzones = Limitzones("limitzones")

        # init states
        states = States()

        # generate signals.
        location_LR = Location()
        location_LR.x = self.limitzones.BL.x
        location_LR.y = self.limitzones.BL.y

        location_RL = Location()
        location_RL.x = self.limitzones.TR.x
        location_RL.y = self.limitzones.TR.y

        self.signals = [
            Signal(Direction.LR, location_LR, states),
            Signal(Direction.RL, location_RL, states)
        ]

        # generate vehicles for each roads.
        vehicles_queues = [road.Vehicles for road in self.roads]

        self.cx = config.client_width
        self.cy = config.client_height

        self.vehicles = VehicleReceiver(vehicles_queues, self.cx, self.cy)

    def initialize_xbee(self):
        return self.vehicles.initialize_xbee()

    def start(self):
        """
        Script to start main test.
        """
        threading.Thread(
            name='vehicle_generate_simulator',
            target=self.vehicles.receive_data,
            args=()).start()

        # delay to make sure there are vehicles generated.
        sleep(2)

        # ==========================================================================================
        # ====== 3 algorithm for comparision and evaluation               ==========================
        # ====== to switch between different algorithm, simply comment / uncomment below lines =====
        # ==========================================================================================

        # dp_client = ARloc(roads, limit zones, signals)
        # threading.Thread(name='ARloc', target=dp_client.run, args=()).start()

        # dp_client = ARla(roads, limit zones, signals)
        # threading.Thread(name='ARla', target=dp_client.run, args=()).start()

        dp_client = DP(self.roads, self.limitzones, self.signals)
        threading.Thread(name='DP', target=dp_client.run, args=()).start()

        # update vehicles location.
        threading.Thread(
            name='vehicle_driving_simulator',
            target=self.vehicles.update_location,
            args=(self.roads, self.limitzones, self.signals)).start()

        return self.vehicles, dp_client

    def stop(self, simulator, dp_client):
        """
        """

        if isinstance(simulator, VehicleReceiver):
            simulator.stop()

        if isinstance(dp_client, (ARloc, ARla, DP)):
            print("--- try to stop dp_client")
            dp_client.stop()


if __name__ == "__main__":
    ctrl = ITSManage()
    ctrl.initialize()
    ctrl.start()
