# -*- coding: utf-8 -*-
from __future__ import print_function

"""Generate dummy vehicle.
"""

from time import sleep
import threading
import string
import random

import sys
import os

root_path = os.sep.join(os.path.dirname(os.path.realpath(__file__)).split(os.sep)[0:-1])
if root_path not in sys.path:
    sys.path.append(root_path)

is_py2 = sys.version[0] == '2'
if is_py2:
    import Queue as queue
else:
    import queue as queue

from models.vehicles import Vehicle
from models.location import Location
from models.direction import Direction
import config
from xbee.EXbee import EXbee


class DummyVechilesSender(object):
    """
    """
    inst_xbee = None

    def __init__(self, vehicles_queues, client_width, client_height):
        """
            -- vehicles_queues: list of vehicles queues, one queue for one road.
            -- client_width: width of client canvas for drawing.
            -- client_height: height of client canvas for drawing.
        """

        self.client_width = client_width
        self.client_height = client_height
        self.vehicles_queues = vehicles_queues

        self.counts = [0, 0, 0, 0, 0]

        self.alive = True

        self.inst_xbee = EXbee(config.xbee_port, config.xbee_baud)

    def generate_all(self):
        """
        """

        while self.alive:
            self.generate_one()
            sleep(random.randint(3, 5))

        print(">>> DummyVechiles.generate_all stopped")

    def stop(self):
        """Stop simulator.
        """
        self.alive = False

    def generate_one(self):
        """Generate a dummy vehicle.
        """

        # random direction -- main road has more traffic than side streets.
        # direction = int(random.randint(0, 10) / 5)
        direction = int(random.randint(0, 6) / 3)

        str_vehicle = 'dir:' + str(direction)
        print(str_vehicle)
        response = self.inst_xbee.send_tx(str_vehicle, config.xbee_master_addr, '02')
        if response is None:
            print('Failed to send data, retrying...')
            response = self.inst_xbee.send_tx(str_vehicle, config.xbee_master_addr, '02')

        if response is None:
            return False
        if response['Delivery_status'][:2] == "00":
            print("Frame sent with success")
        else:
            print("Failed to send data...")

        # if direction is 0 or 1, the vehicle is on main road
        if direction in [0, 1]:
            queue_index = direction
            # if vehicles queue at this direction is full, ignore.
            if self.vehicles_queues[queue_index].full():
                print("--- vehicles queue %s is full." % Direction.get_name(queue_index))
                return
        else:
            # random added to one of 3 side roads.
            road_index = random.randint(0, config.count_sideroads - 1)

            # if vehicles queue at this direction is full, ignore.
            queue_index = direction + road_index
            if self.vehicles_queues[queue_index].full():
                print("--- vehicles queue %s is full." % Direction.get_name(queue_index))
                return

        location = self.get_enter_location(queue_index)

        name = self.inst_xbee.get_device_id()
        vehicle = Vehicle(name, direction, location, queue_index)

        # by default, vehicle is stopped at the moment of generation.
        # vehicle should update it's speed (move or stop) at every DP iterator.
        vehicle.speed = 0

        # self.vehicles_queues[queue_index].put(vehicle)
        self.counts[direction] += 1

        print("--- LR:(%d) RL:(%d)" % (self.vehicles_queues[0].qsize(), self.vehicles_queues[1].qsize()))
        print("Sideway_1:(%d) Sideway_2:(%d) Sideway_3:(%d)" % (
            self.vehicles_queues[2].qsize(), self.vehicles_queues[3].qsize(), self.vehicles_queues[4].qsize()))

    def get_enter_location(self, queue_index):
        """Generate enter location by queue_index.
           We assume main road is at screen's center height.

           queue_index:
             -- 0: main road from west to east
             -- 1: main road from east to west
             -- 2: side road 1
             -- 3: side road 2
             -- 4: side road 3
        """

        # print "--- queue_index =>", queue_index

        location = Location()
        if 0 == queue_index:
            # main road from left to right
            location.x = 30
            location.y = self.client_height * 0.5
        elif 1 == queue_index:
            # main road from right to left
            location.x = self.client_width - 30
            location.y = self.client_height * 0.5
        else:
            # side roads
            sideway_start_x = config.length_road_left * Location.unit_x()
            sideway_gap = (
                config.length_limitzones - config.width_side * config.count_sideroads
            ) / (config.count_sideroads - 1) * Location.unit_x()

            location.x = sideway_start_x + (sideway_gap + config.width_side * Location.unit_x()) * (queue_index - 2)
            location.y = 30
            # print "sideway location =>", location

        return location

    def update_location(self, roads, limitzones, signals):
        """Update vehicles location in a separated thread.
        """

        from config import update_interval

        while self.alive:

            vehicles = []

            for road in roads:
                vs = list(road.Vehicles.queue)
                vehicles.extend(vs)

            for vehicles_queue in limitzones.vehicles_queues:
                vehicles.extend(vehicles_queue.queue)

            vs = list(limitzones.leave_queue.queue)
            vehicles.extend(vs)

            for v in vehicles:
                v.update_location(roads, limitzones, signals)

            sleep(update_interval)

        print(">>> DummyVechiles.update_location stopped")


def start_simulator(vehicles_queues, client_width, client_height):
    """
    """

    simulator = DummyVechilesSender(
        vehicles_queues, client_width, client_height
    )

    simulator.inst_xbee.initialize()

    t = threading.Thread(
        name='vehicle_generate_simulator',
        target=simulator.generate_all,
        args=())

    t.start()


if __name__ == "__main__":
    from config import client_width, client_height, count_sideroads

    # generate queues to save vehicles for each roads.
    max_vehicles = 50

    # generate vehicles for main roads and 3 side roads.
    #   -- queue 0: main road from west to east
    #   -- queue 1: main road from east to west
    #   -- queue 2: side road 1
    #   -- queue 3: side road 2
    #   -- queue 3: side road 3
    vehicles_queues = [queue.Queue(max_vehicles) for i in range(2 + count_sideroads)]

    start_simulator(vehicles_queues, client_width, client_height)
