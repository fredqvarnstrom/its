# Copyright ConstrucTRON Inc, 2016

# -*- coding: utf-8 -*-
"""ARloc (Adaptive local control) algorithm.

   Only consider vehicles that already arrived and are waiting.
"""

from __future__ import division

import threading

from models.signals import States
import config

from algorithm_base import AlgorithmBase


class ARloc(AlgorithmBase):
    """ARloc algorithm.
    """

    def wait_time_change(self):
        """Wait time (if direction change) includes:
             -- dring time
             -- clearance time at t0 (decision moment)
        """

        current_state = self.states.current_state

        count_LR = self.roads[0].get_waiting_count()
        count_RL = self.roads[1].get_waiting_count()

        C = self.limitzones.get_cleartime()

        if current_state == States.LR_ENTER:
            #=======================================================================
            #== current direction is left -> right, change to right -> left ========
            #=======================================================================

            # vehicles in right queues will have to wait at least:
            # 1. current left vehicles that already entered limitzones to leave,
            #    e.g. left -> right direction clearance time
            wait_time_RL = count_RL * C

            # vehicles in left queues will have to wait at least:
            # 1. current left vehicles that already entered limitzones to leave,
            #    e.g. left -> right direction clearance time
            # 2. vehicles in right queue enter and pass limitzones
            T_LR = self.roads[1].get_drivetime()
            wait_time_LR = count_LR * (C + T_LR)

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_LR += config.minimum_wait_time * max(count_RL * C - 1, 0)

            return max(wait_time_RL, wait_time_LR)

        elif current_state == States.RL_ENTER:
            #=======================================================================
            #== current direction is right -> left, change to left -> right ========
            #=======================================================================

            # vehicles in right queues will have to wait at least:
            # 1. current right vehicles that already entered limitzones to leave,
            #    e.g. right -> left direction clearance time
            # 2. vehicles in left enter and pass limitzones
            T_RL = self.roads[0].get_drivetime()
            wait_time_RL = count_RL * (C + T_RL)

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_RL += config.minimum_wait_time * max(count_LR * C - 1, 0)

            # vehicles in WE queues will have to wait at least:
            # 1. current EW vehicles that already entered limitzones to leave, e.g. EW clearance time
            wait_time_LR = count_LR * C

            return max(wait_time_RL, wait_time_LR)

        # for other value of current_state, don't allow change state.
        return float('inf')

    def wait_time_nochange(self):
        """Wait time (if no direction change) is deference of:
             -- the new clearance time caused by new vehicle entering limitzones
                from currently allowed direction. this value is equals to driving time.
             -- clearance time at t0 (decision moment)
        """

        current_state = self.states.current_state

        count_LR = self.roads[0].get_waiting_count()
        count_RL = self.roads[1].get_waiting_count()

        C = self.limitzones.get_cleartime()

        if current_state == States.LR_ENTER:
            #=================================================
            #== current direction is left -> right
            #=================================================

            # vehicles in right queues will have to wait increased wait time
            T_LR = self.roads[0].get_drivetime()
            wait_time_RL = count_RL * (T_LR - C)

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_RL += config.minimum_wait_time * max(count_LR * C - 1, 0)

            # vehicles in left queues will have no wait time.
            wait_time_LR = 0

            return max(wait_time_RL, wait_time_LR)

        elif current_state == States.RL_ENTER:
            #=================================================
            #== current direction is right -> left
            #=================================================

            # vehicles in right queues will have no wait time.
            wait_time_RL = 0

            # vehicles in left queues will have to wait increased wait time
            T_RL = self.roads[1].get_drivetime()
            wait_time_LR = count_LR * (T_RL - C)

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_LR += config.minimum_wait_time * max(count_RL * C - 1, 0)

            return max(wait_time_RL, wait_time_LR)

        # for other value of current_state, don't allow change state.
        return float('inf')

    def get_next_direction(self):
        """
        """

        current_state = int(self.states.current_state)

        # calculate wait time for 2 options.
        wchange = self.wait_time_change()
        wnochange = self.wait_time_nochange()

        if wchange > wnochange:
            return current_state

        if current_state == States.LR_ENTER:
            return States.RL_ENTER

        if current_state == States.RL_ENTER:
            return States.LR_ENTER

        # this situation should not happen.
        return States.INIT


def start_ARloc(roads, limitzones, states):
    dp_client = ARloc(roads, limitzones, states)

    t = threading.Thread(name='ARloc', target=dp_client.run, args=())
    t.start()
