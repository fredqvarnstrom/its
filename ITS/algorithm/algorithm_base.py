# Copyright ConstrucTRON Inc, 2016

# -*- coding: utf-8 -*-
"""Base class for algorithm.
"""

from __future__ import division

from time import sleep

from models.direction import Direction
from models.signals import States, Color
import config
import rules


class AlgorithmBase(object):
    """
    """

    def __init__(self, roads, limitzones, signals):
        """Define data structure for meta result saving.
        """

        self.roads = roads
        self.limitzones = limitzones
        self.signals = signals
        self.states = signals[0].states

        self.cleartime = 0.0
        self.total_wait_time = 0.0

        self.alive = True

    def solve(self):
        """Solve: get min average wait time for vehicles, consider a sequence of direction changes.
        """

        current_state = int(self.states.current_state)

        # how long for next vehicle will arrive at both side?
        next_arrival_times = [self.roads[direction].get_next_vehicle_arrival_time() for direction in [
            Direction.LR, Direction.RL
        ]]

        # how many vehicles at both side?
        vehicle_counts = [self.roads[direction].Vehicles.qsize() for direction in [
            Direction.LR, Direction.RL
        ]]

        # if current_state is init, handle init rules.
        if current_state == States.INIT:
            rules.handle_init_state(next_arrival_times, vehicle_counts, self.signals)
            return

        clearance_time = self.limitzones.get_cleartime()

        # if current_state not allow either side enter, it is waiting limit zones to be cleared.
        if current_state not in [States.RL_ENTER, States.LR_ENTER]:
            if clearance_time <= 0:
                self.change_state()
                print ">>> changed state. clearance_time =>", clearance_time,
                print "current_state =>", current_state
                print "limitzones.vehicles_queues[0] =>", self.limitzones.vehicles_queues[0].qsize()
                print "limitzones.vehicles_queues[1] =>", self.limitzones.vehicles_queues[1].qsize()

            return

        # if current_state is either side enter.
        driving_time = config.length_limitzones / config.speed_vehicle

        # if current_state is enter, we have some force change direction rules.
        force_change = rules.handle_extra_rules(
            driving_time, clearance_time, current_state, next_arrival_times
        )

        if force_change:
            self.change_state()
            return

        decision_moment = rules.is_decision_moment(
            current_state, next_arrival_times, vehicle_counts)

        if decision_moment:
            # calculate wait time and new direction.
            direction = int(self.get_next_direction())

            if (
                (direction == Direction.LR and current_state != States.LR_ENTER) or
                (direction == Direction.RL and current_state != States.RL_ENTER)
            ):
                # change the direction.
                self.change_state()

    def change_state(self):
        """
        """
        self.states.change_state()
        for signal in self.signals:
            signal.update()

    def run(self):
        """
        """

        print "Algorithm is running."
        interval = config.minimum_decision_interval

        yellow_duration = config.yellow_duration
        green_duration = config.minimum_green_duration

        while self.alive:

            duration = self.signals[0].get_duration()
            need_decision = True

            for signal in self.signals:

                # ensure green last at least 5 seconds.
                if signal.color == Color.GREEN and duration < green_duration:
                    need_decision = False
                    break

                # ensure yellow last fixed 5 seconds.
                elif signal.color == Color.YELLOW and duration < yellow_duration:
                    need_decision = False
                    break

            if need_decision:
                self.solve()

            sleep(interval)

        print ">>> Algorithm stopped"

    def stop(self):
        """Stop Algorithm.
        """

        self.alive = False
