# Copyright ConstrucTRON Inc, 2016

# -*- coding: utf-8 -*-
"""Rules for algorithm.

   deal with extra rules for decision moment.
"""


from models.direction import Direction
from models.signals import States
import config


def is_decision_moment(current_state, next_arrival_times, vehicle_counts):
    """Check if it should be a decision moment.

       add this rule to allow all arrived vehicles on one side can enter limitzone at once.
    """

    # wait seconds for coming vehicle to arrive end of road.
    wait = config.green_wait_min

    # we want one side to be fully cleared.
    if (
        current_state == States.LR_ENTER and
        next_arrival_times[Direction.LR] > wait and
        vehicle_counts[Direction.LR] * 2 < vehicle_counts[Direction.RL]
    ):
        print "|||^^^ left switch. next_arrival_times[Direction.LR] =>", next_arrival_times[Direction.LR]
        return True

    if (
        current_state == States.RL_ENTER and
        next_arrival_times[Direction.RL] > wait and
        vehicle_counts[Direction.RL] * 2 < vehicle_counts[Direction.LR]
    ):
        print "|||^^^ right switch. next_arrival_times[Direction.RL] =>", next_arrival_times[Direction.RL]
        return True

    return False


def handle_init_state(next_arrival_times, vehicle_counts, signals):
    """Rule: by default, both side should keep red signals until vehicles
             to be detected at either side.

       Logic:
          -- if both side has waiting vehicles, allow longer queue enter first
          -- otherwise, if one side has vehicle arriaved the end of road first, allow it enter.
          -- otherwise, keep both signals red.
    """

    new_state = States.INIT

    # if both side has waiting vehicles, allow longer queue enter first
    if max(next_arrival_times) < 1:
        if vehicle_counts[Direction.LR] > vehicle_counts[Direction.RL]:
            new_state = States.LR_ENTER
        else:
            new_state = States.RL_ENTER

    # if one side has waiting vehicles, allow it enter first
    elif next_arrival_times[Direction.LR] == 0:
        new_state = States.LR_ENTER
    elif next_arrival_times[Direction.RL] == 0:
        new_state = States.RL_ENTER

    for i in [Direction.LR, Direction.RL]:
        signals[i].states.current_state = new_state
        signals[i].update()


def handle_extra_rules(driving_time, clearance_time, current_state, next_arrival_times):
    """Rule:

       1. turn to yellow signal light if clearance time is within last 10%
       2. if green last more than 5 seconds, and no vehicle arrival within next green_wait_max seconds
          force to turn on yellow to reduce another side wait time.
    """

    if current_state not in [States.LR_ENTER, States.RL_ENTER]:
        return False

    need_change = False

    # check if need to switch to yellow.
    if (
        driving_time > 0 and clearance_time > 0 and
        clearance_time / driving_time < 0.1
    ):
        # turn yellow signal light if clearance time is within last 10%
        need_change = True
    elif (
        (current_state == States.LR_ENTER and next_arrival_times[Direction.LR] > config.green_wait_max) or
        (current_state == States.RL_ENTER and next_arrival_times[Direction.RL] > config.green_wait_max)
    ):
        # if green last more than 5 seconds, and no vehicle arrival within next green_wait_max seconds
        # force to turn on yellow to reduce another side wait time.
        need_change = True

    return need_change
