# Copyright ConstrucTRON Inc, 2016

# -*- coding: utf-8 -*-
"""DP algorithm.
"""

from __future__ import division

import threading

from models.direction import Direction
from models.signals import States

import config

from algorithm_base import AlgorithmBase


class DP(AlgorithmBase):
    """DP algorithm.
    """

    def wait_time_change(self, current_state, t):
        """Wait time (if direction change) includes:
             -- dring time
             -- clearance time at t0 (decision moment)
             -- negative future arrival time
        """

        count_LR = self.roads[0].get_waiting_count()
        count_RL = self.roads[1].get_waiting_count()

        # arrival time means ti - t0 where:
        #   -- t0: the decision moment
        #   -- ti: the moment ith vehicle will arrive
        # if vehicle not arrival yet, ti is a future time and always larger than t0.
        arrival_LR = self.roads[0].get_future_arrival_time(t)
        arrival_RL = self.roads[1].get_future_arrival_time(t)

        C = self.limitzones.get_cleartime()

        if current_state == States.LR_ENTER:
            #=======================================================================
            #== current direction is left -> right, change to right -> left ========
            #=======================================================================

            # vehicles in right queues will have to wait at least:
            # 1. current left vehicles that already entered limitzones to leave,
            #    e.g. left -> right direction clearance time
            wait_time_RL = count_RL * C - arrival_RL

            # vehicles in left queues will have to wait at least:
            # 1. current left vehicles that already entered limitzones to leave,
            #    e.g. left -> right direction clearance time
            # 2. vehicles in right queue enter and pass limitzones
            # 3. vehicles that will arrive left.
            T_LR = self.roads[1].get_drivetime()
            wait_time_LR = count_LR * (C + T_LR) - arrival_LR

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_LR += config.minimum_wait_time * max(count_RL * C - 1, 0)

            return max(wait_time_LR, wait_time_RL, 0)

        elif current_state == States.RL_ENTER:
            #=======================================================================
            #== current direction is right -> left, change to left -> right ========
            #=======================================================================

            # vehicles in right queues will have to wait at least:
            # 1. current right vehicles that already entered limitzones to leave,
            #    e.g. right -> left direction clearance time
            # 2. vehicles in left enter and pass limitzones
            T_RL = self.roads[0].get_drivetime()
            wait_time_RL = count_RL * (C + T_RL) - arrival_RL

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_RL += config.minimum_wait_time * max(count_LR * C - 1, 0)

            # vehicles in WE queues will have to wait at least:
            # 1. current EW vehicles that already entered limitzones to leave, e.g. EW clearance time
            wait_time_LR = count_LR * C - arrival_LR

            return max(wait_time_LR, wait_time_RL, 0)

        # for other value of current_state, don't allow change state.
        return float('inf')

    def wait_time_nochange(self, current_state, t):
        """Wait time (if no direction change) is deference of:
             -- the new clearance time caused by new vehicle entering limitzones
                from currently allowed direction. this value is equals to driving time.
             -- clearance time at t0 (decision moment)
             -- future arrival time
        """

        count_LR = self.roads[0].get_waiting_count()
        count_RL = self.roads[1].get_waiting_count()

        arrival_LR = self.roads[0].get_future_arrival_time(t)
        arrival_RL = self.roads[1].get_future_arrival_time(t)

        C = self.limitzones.get_cleartime()

        if current_state == States.LR_ENTER:
            #=================================================
            #== current direction is left -> right
            #=================================================

            # vehicles in right queues will have to wait increased wait time
            T_LR = self.roads[0].get_drivetime()
            wait_time_RL = count_RL * (T_LR - C) + arrival_RL

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_RL += config.minimum_wait_time * max(count_LR * C - 1, 0)

            # vehicles in left queues will need arrival time.
            wait_time_LR = arrival_LR

            return max(wait_time_LR, wait_time_RL, 0)

        elif current_state == States.RL_ENTER:
            #=================================================
            #== current direction is right -> left
            #=================================================

            # vehicles in right queues will have no wait time.
            wait_time_RL = arrival_RL

            # vehicles in left queues will have to wait increased wait time
            T_RL = self.roads[1].get_drivetime()
            wait_time_LR = count_LR * (T_RL - C) + arrival_LR

            # take account minimum waiting time for vehicles in a queue to enter into limitzones.
            wait_time_LR += config.minimum_wait_time * max(count_RL * C - 1, 0)

            return max(wait_time_LR, wait_time_RL, 0)

        # for other value of current_state, don't allow change state.
        return float('inf')

    def get_next_direction(self):
        """Get next optimal direction.
        """

        n = config.num_states

        sub_wait = range(n+1)   # saved wait time for subsolution
        sub_s    = range(n+1)   # saved subsolution
        wait     = 0            # final wait time
        s        = 0            # final solution: which direction should be go first.

        current_state = int(self.states.current_state)

        time_change = self.wait_time_change(current_state, 0)
        time_nochange = self.wait_time_nochange(current_state, 0)

        if time_change < time_nochange:
            sub_wait[0] = time_change
            if current_state == States.LR_ENTER:
                sub_s[0] = States.RL_ENTER
            else:
                sub_s[0] = States.LR_ENTER
        else:
            sub_wait[0] = time_nochange
            if current_state == States.LR_ENTER:
                sub_s[0] = States.LR_ENTER
            else:
                sub_s[0] = States.RL_ENTER

        # how long for next sub-decision moment.
        interval = config.minimum_decision_interval

        for j in range(1, n+1):
            time_change = self.wait_time_change(sub_s[j-1], j * interval)
            time_nochange = self.wait_time_nochange(sub_s[j-1], j * interval)

            if time_change < time_nochange:
                sub_wait[j] = sub_wait[j-1] + time_change
                if sub_s[j-1] == States.LR_ENTER:
                    sub_s[j] = States.RL_ENTER
                else:
                    sub_s[j] = States.LR_ENTER
            else:
                sub_wait[j] = sub_wait[j-1] + time_nochange
                if sub_s[j-1] == States.LR_ENTER:
                    sub_s[j] = States.LR_ENTER
                else:
                    sub_s[j] = States.RL_ENTER

        wait = sub_wait[n]

        if sub_s[n] == States.LR_ENTER:
            s = Direction.LR
        else:
            s = Direction.RL

        print "DP decision =>", s, "wait =>", wait
        return s


def start_DP(roads, limitzones, states):
    dp_client = DP(roads, limitzones, states)

    t = threading.Thread(target=dp_client.run, args=())
    t.start()
