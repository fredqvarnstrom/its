#! /usr/bin/python
import EXbee
kk = EXbee.EXbee('/dev/ttyUSB0', 9600)

# When XBee module is plugged in, we need to initialize it.
# Once it is initialized, no need to initialize again until it is unplugged.
kk.initialize()

# To get Device ID, call the function below.
print kk.get_device_id()

# To execute AT command:
kk.execute_at('PL', '03', is_ascii=False)

# To execute remote AT command:
kk.send_remote_at("SL", "0013A20040DB7BA6")

# To send data packet to the remote node:
response = kk.send_tx("Hi, This is End Device", "0013A20040DB7BA6", "02")
if response['Delivery_status'][:2] == "00":
    print "Frame sent with success"
else:
    print "Failed"

# To receive data from the remote node:
response = kk.read_rx()
print "Sender:%s     " % response['SOURCE_ADDRESS_64']
print "Message: %s   " % response['DATA']
