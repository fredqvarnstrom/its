from __future__ import print_function

import EXbee

kk = EXbee.EXbee('/dev/ttyUSB0', 9600)

# kk.initialize()

id_hex = kk.get_device_id()

print('Device ID: ', id_hex)

print ('64 bit address: ', kk.get_64bit_addr())

# response = kk.send_tx("Hi, This is End Device", "0013a20040d91ab0", "02")         # Signal 2
# response = kk.send_tx("Hi, This is End Device", "0013a20040d91aaf", "02")           # Signal 1
response = kk.send_tx("Hi, This is End Device", "0013a20040d91ab1", "02")         # Node

print(response)


