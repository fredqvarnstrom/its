#! /usr/bin/python
from __future__ import print_function
import EXbee

kk = EXbee.EXbee('COM4', 9600)

print(kk.initialize())

print(kk.execute_at("FN"))

print(kk.get_device_id())

print(kk.find_neighbors())

# print kk.execute_at('PL', '03', is_ascii=False)

# kk.set_as_end_device('1234', 'My End Device')
