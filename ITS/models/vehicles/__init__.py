# -*- coding: utf-8 -*-
"""vehicles package.
"""

from vehicle import Vehicle

__all__ = [
    'Vehicle',
]