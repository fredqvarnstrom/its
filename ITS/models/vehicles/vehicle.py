# -*- coding: utf-8 -*-
"""Vehicle.
"""

from __future__ import division

from models.direction import Direction
from models.location import Location
from models.signals import Color
import random
import config


class Vehicle(object):
    """Vehicle class deal with vehicle's status, location, move forward or stop for waiting.
    """

    _speed = 0.0

    def __init__(self, name, direction, location, queue_index):
        """
           queue_index:
             -- 0: main road from left to right
             -- 1: main road from right to left
             -- 2: side road 1
             -- 3: side road 2
             -- 4: side road 3
        """

        self.name = name
        self.direction = int(direction)
        self.queue_index = int(queue_index)
        self.location = location

        if self.direction == Direction.TB and queue_index > 1:
            self.road_type = 'sideway'
        else:
            self.road_type = 'mainway'

        # vehicle's status: 0 -- not enter limitzones, 1 -- entered limitzones, 2 -- passed crossroad
        self.status = 0

        # if vehicle in waiting queue -- we need to distinguash vehicle's arrival stage:
        #   -- if vehicle has arrived and stopped for waiting enter limitzone, this value will be True,
        #   -- otherwise, if vehicle in road's queue but still driving to the end of road, it is False
        self.waiting = False

    def __str__(self):
        """
        """
        return "name: %s, direction: %s, location %s, status %s" % (
            self.name, Direction.get_name(self.direction), self.location, self.status)

    @property
    def speed(self):
        """in real industry, speed should be collected from exteral device, never modify by the software
           so make it is private, only be able to read via property.
        """
        return self._speed

    @speed.setter
    def speed(self, val):
        """for the demo, we need to modify vehicle's speed by programming, so allow this setter property.

           should be never called when used in real industry.
        """
        self._speed = val

    def get_next_location(self):
        """Calculate next location for the vehicle.

           distance to next location is vehicle's speed * update_interval.
        """

        direction = int(self.direction)

        speed = self.speed
        if 0 == speed:
            # when calculate next location, we need to assume the vehicle has speed.
            speed = config.speed_vehicle

        # convert m/s speed to pixel/s speed.
        if direction in [Direction.TB, Direction.BT]:
            speed_pixel = speed * Location.unit_y()
        else:
            speed_pixel = speed * Location.unit_x()

        driving_distance = speed_pixel * config.update_interval

        loc = Location()

        # moving from top to bottom (limit zone).
        if direction == Direction.TB:
            loc.x = self.location.x
            loc.y = self.location.y + driving_distance

        # moving from bottom (limit zone) to top.
        elif direction == Direction.BT:
            loc.x = self.location.x
            loc.y = self.location.y - driving_distance

        # moving from left to right.
        elif direction == Direction.LR:
            loc.x = self.location.x + driving_distance
            loc.y = self.location.y

        # moving from right to left.
        elif direction == Direction.RL:
            loc.x = self.location.x - driving_distance
            loc.y = self.location.y

        else:
            loc.x = self.location.x
            loc.y = self.location.y

        # adjust location.y if the vehicle is from sideway and turn on to main way.
        if (
            self.road_type == 'sideway' and self.status > 0 and
            direction in [Direction.LR, Direction.RL]
        ):
            loc.y = config.client_height * 0.5

        return loc

    def get_previous_vehicle(self, queue):
        """Get the vehicle that is front of this vehicle.

           Only use for roads queue and leaving queue, don't use
           for limitzones queues as limitzones queues is actually list based.
           vehicles in limitzones queues not order by locations.
           consider vehicles may join limitzones from sideways.

           Actually, this function is only used for roads queues currently,
           as we need to have vehicles stop at end of road to wait enter limitzones,
           so need to check if previous vehicle already stopped.

           For leaving queues and limitzones queues, vehicles no need to stop,
           just moving to next location.
        """

        pos = -1

        vs = list(queue.queue)
        vehicles = [(i, v) for (i, v) in enumerate(vs)]
        for (i, v) in vehicles:
            if v.name == self.name:
                pos = i

        if pos > 0:
            return vs[pos - 1]
        else:
            return None

    def conflict_location(self, location1, location2):
        """Check if any other vehicles occured or overlap with the location.
        """

        result = False
        direction = int(self.direction)

        # convert length_vehicle to pixel.
        if direction in [Direction.TB, Direction.BT]:
            length_vehicle_pixel = int(config.length_vehicle * Location.unit_y())
            if int(abs(location1.y - location2.y)) < length_vehicle_pixel:
                result = True
        else:
            length_vehicle_pixel = int(config.length_vehicle * Location.unit_x())
            if int(abs(location1.x - location2.x)) < length_vehicle_pixel:
                result = True

        return result

    def driving_time_to(self, target_location):
        """Calculate estimated driving time from vehicle's current location to target_location.

           Logic:
             -- if the vehicle has valid speed, use it, otherwise,
             -- use default vehicle speed for calculation.
        """

        distance_pixel = Location.calculate_distance(target_location, self.location)

        if self.speed == 0:
            self.speed = config.speed_vehicle

        # convert m/s speed to pixel/s speed.
        if self.direction in [Direction.TB]:
            speed_pixel = self.speed * Location.unit_y()
        else:
            speed_pixel = self.speed * Location.unit_x()

        return distance_pixel / speed_pixel

    def update_sideway(self, roads, limitzones, signals):
        """Update vehicle's location on sideway.

           Logic:
             -- if the vehicle is the first vehicle in road's queue
                -- if it arrived end of sideway, generate either LR or RL direction for it.
                -- otherwise, move forward

             -- if the vehicle is not the first vehicle in road's queue
                -- if there is enough length for it to move to next location, move it (update location),
                   e.g. there is another vehicle occured the next location.
                -- otherwise, stop (no update location needed)
        """

        name = self.name
        queue_index = int(self.queue_index)

        road = roads[queue_index]
        road_queue = road.Vehicles

        # if it is the first vehicle in roads queue
        if name == road_queue.queue[0].name:
            # check if the first vehicle arrived the end of road.
            if not road.arrived_road_end(self.location):
                # if not arrived the end of the road, move it to next location.
                self.location = self.get_next_location()
                self.speed = config.speed_vehicle
            else:
                direction = random.randint(0, 1)
                limitzones_queue = limitzones.vehicles_queues[direction]
                limitzones_queue_otherway = limitzones.vehicles_queues[(direction + 1) % 2]

                # vehicle from sideway allow to enter limitzone if same direction is passing.
                if (
                    limitzones_queue_otherway.empty() and
                    not limitzones_queue.full() and
                    not limitzones_queue.empty()
                ):
                    # vehicle from sideway don't allow enter at last minute.
                    clearance_time = limitzones.get_cleartime()

                    if direction == Direction.LR:
                        driving_time = self.driving_time_to(limitzones.BR)
                    else:
                        driving_time = self.driving_time_to(limitzones.TL)

                    # don't enter if increasing clearance_time > 10%.
                    if clearance_time > 0 and driving_time / clearance_time < 0.9:
                        # leave current main road.
                        v = road_queue.get()
                        # update location
                        v.location = v.get_next_location()
                        # update status
                        v.status = 1
                        # check direction
                        v.direction = direction
                        # enter limitzones
                        limitzones_queue.put(v)
                else:
                    # limitzones full or signal not allowed, stop
                    self.speed = 0
        elif not road.arrived_road_end(self.location):
            # check if enough length for the vehicle to move forward.
            loc = self.get_next_location()
            front_v = self.get_previous_vehicle(road_queue)

            if not self.conflict_location(loc, front_v.location):
                self.location = loc
                self.speed = config.speed_vehicle
            else:
                # location conflicted, stop
                self.speed = 0

    def update_location(self, roads, limitzones, signals):
        """Update vehicle's location on mainway.

           Logic:
             -- if the vehicle is the first vehicle in road's queue
                -- if allow the vehicle's direction enter limitzones, update location and move to limitzones queue
                -- otherwise, stop (no update location needed).

             -- if the vehicle is not the first vehicle in road's queue
                -- if there is enough length for it to move to next location, move it (update location),
                   e.g. there is another vehicle occured the next location.
                -- otherwise, stop (no update location needed)

             -- if the vehicle in limitzones
                -- if it's next location outside of the limitzones (means it passed limitzones),
                   move it to leave_queue.
        """

        status = int(self.status)
        direction = int(self.direction)

        # only allow LR and RL direction vehicles enter limitzones
        if status == 0 and self.road_type == 'sideway':
            return self.update_sideway(roads, limitzones, signals)

        # if the vehicle passed limitzones, continue driving to leave.
        elif status == 2:
            return self.leave(limitzones.leave_queue)

        name = self.name

        signal_color = signals[direction % 2].color
        limitzones_queue = limitzones.vehicles_queues[direction]
        limitzones_queue_otherway = limitzones.vehicles_queues[(direction + 1) % 2]

        road = roads[direction]
        road_queue = road.Vehicles
        leave_queue = limitzones.leave_queue

        road_vehicles = list(road_queue.queue)

        # if the vehicle is driving on main road
        if status == 0:
            # if it is the first vehicle in roads queue
            if name == road_vehicles[0].name:
                # check if the first vehicle arrived the end of road.
                if not road.arrived_road_end(self.location):
                    # if not arrived the end of the road, move it to next location.
                    self.location = self.get_next_location()
                    self.speed = config.speed_vehicle

                # if limitzones_queue is not full and the direction allow to enter.
                elif (
                    limitzones_queue_otherway.empty() and
                    not limitzones_queue.full() and
                    signal_color == Color.GREEN
                ):
                    # leave current main road.
                    v = road_queue.get()
                    # update location
                    v.location = v.get_next_location()
                    # update status
                    v.status = 1
                    # enter limitzones
                    limitzones_queue.put(v)
                else:
                    # limitzones full or signal not allowed, stop
                    self.speed = 0
                    self.waiting = True

            # it is other vehicle on main road
            elif not road.arrived_road_end(self.location):
                # check if enough length for the vehicle to move forward.
                loc = self.get_next_location()
                front_v = self.get_previous_vehicle(road_queue)

                if front_v:
                    # if previous vehicle is driving, or previous vehicle is wait but there is
                    # enough length for this vehicle to move forward, move it.
                    if not front_v.waiting or not self.conflict_location(loc, front_v.location):
                        self.location = loc
                        self.speed = config.speed_vehicle
                    else:
                        # have this vehicle stop and waiting.
                        self.speed = 0
                        self.waiting = True
                else:
                    # location conflicted, stop
                    self.speed = 0
                    self.waiting = True

        # if the vehicle is driving on limitzones
        elif status == 1:

            # if the vehicle leaves limitzones, move it to leave_queue.
            if not limitzones.inside_limitzones(self):
                pos = limitzones_queue.index(name)

                if 0 <= pos < limitzones_queue.qsize():
                    print ">>> enter leave_queue along main road.", self

                    # leave limitzones
                    v = limitzones_queue.get(pos)
                    # update status
                    v.status = 2
                    # update location
                    v.location = v.get_next_location()
                    # enter leave queue
                    leave_queue.put(v)
                    limitzones_queue.pop(pos)
                else:
                    print "============= whoops, wrong vehicle position in limitzones", self

            else:
                # check if arrive one of enter of side street
                if direction in [Direction.LR, Direction.RL]:
                    # turn down to sideway in a random frequency.
                    self.turn_to_sideway(roads[2:], limitzones_queue, leave_queue)

                # move to next location
                self.location = self.get_next_location()
                self.speed = config.speed_vehicle

    def turn_to_sideway(self, sideways, limitzones_queue, leave_queue):
        """if a vehicle arrived one of side streets enter along main road right -> left direction,
           it can leave limitzones by turn down to this side street.

           We use a random frequency to allow a vehicle turn down to a sideway.
        """

        for sideway in sideways:
            if sideway.arrived_side_enter(self.location):
                rd = random.randint(0, 30)
                # lower chance it turn down to sideway.
                if rd < 30:
                    continue

                # turn down to side street
                pos = limitzones_queue.index(self.name)

                if 0 <= pos < limitzones_queue.qsize():
                    print ">>> enter leave_queue along sideway.", self

                    # leave limitzones
                    v = limitzones_queue.get(pos)
                    # update status
                    v.status = 2
                    # update location
                    v.direction = Direction.BT
                    v.location.x = sideway.location.x + 80
                    v.location = v.get_next_location()
                    # enter leave queue
                    leave_queue.put(v)
                    # remove from limitzones_queue
                    limitzones_queue.pop(pos)
                else:
                    print "============= whoops, wrong vehicle postion in limitzones", self

                break

    def leave(self, leave_queue):
        """A vehicle passed limitzones, allow it continue driving to out of screen.
           When it drives out of screen, remove it.
        """

        if leave_queue.empty():
            return

        direction = int(self.direction)

        # if driving out of screen viewport, remove it.
        if (
            direction == Direction.LR and self.location.x > config.client_width or
            direction == Direction.RL and self.location.x < 0
        ):
            leave_queue.get()
            # print "*** leave"
        else:
            # move to next location
            self.location = self.get_next_location()
            self.speed = config.speed_vehicle
