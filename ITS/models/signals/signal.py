# -*- coding: utf-8 -*-
"""Class for signal.
"""

from datetime import datetime
from models.direction import Direction
from states import States
import config


class Color(object):
    """
    """

    GREEN = 0
    YELLOW = 1
    RED = 2

    UNKNOWN = -1  # for some reason indicting error occurs


class Signal(object):
    """
    """

    def __init__(self, direction, location, states):
        """
        """

        self.direction = direction
        self.location = location
        self.states = states
        self.color = Color.RED
        self.startat = datetime.utcnow()
        self.duration = 0

    def update(self):
        """Update signal color and duration.
        """

        current_state = int(self.states.current_state)
        direction = int(self.direction)
        self.startat = datetime.utcnow()

        # RL signal
        if direction == Direction.RL:
            # RL_ENTER
            if current_state == States.RL_ENTER:
                self.color = Color.GREEN
            # RL_WAIT
            elif (
                current_state == States.RL_WAIT and
                self.get_duration() < config.yellow_duration and
                self.color != Color.RED
            ):
                self.color = Color.YELLOW
            else:
                self.color = Color.RED
        elif direction == Direction.LR:
            # LR_ENTER
            if current_state == States.LR_ENTER:
                self.color = Color.GREEN
            # LR_WAIT
            elif (
                current_state == States.LR_WAIT and
                self.get_duration() < config.yellow_duration and
                self.color != Color.RED
            ):
                self.color = Color.YELLOW
            else:
                self.color = Color.RED

    def get_duration(self):
        """Return signal color duration since last changed.
        """

        return (datetime.utcnow() - self.startat).total_seconds()
