# -*- coding: utf-8 -*-
"""DP algorithm.
"""

from datetime import datetime


class States(object):
    """Define a set of states and transfer functions for DP algorithm.

       States transfer matrix (1 means can be transfered, otherwise cannot transfered):
       states in left means from_state, states in top means to_state.
       e.g. INIT state can be transfered to either RL_ENTER or LR_ENTER.
       ---------|---------|----------|------------|----------|----------|-----------|----------
                |  INIT   | RL_WAIT  | RL_ENTER   | RL_STOP  | LR_WAIT  | LR_ENTER  | LR_STOP
       ---------|---------|----------|------------|----------|----------|-----------|----------
       INIT     |         |          |   1        |          |          |   1       |
       ---------|---------|----------|------------|----------|----------|-----------|----------
       RL_WAIT  |         |          |   1        |          |          |           |
       ---------|---------|----------|------------|----------|----------|-----------|----------
       RL_ENTER |         |          |            |   1      |          |           |
       ---------|---------|----------|------------|----------|----------|-----------|----------
       RL_STOP  |         |          |            |          |     1    |           |
       ---------|---------|----------|------------|----------|----------|-----------|----------
       LR_WAIT  |         |          |            |          |          |   1       |
       ---------|---------|----------|------------|----------|----------|-----------|----------
       LR_ENTER |         |          |            |          |          |           |  1
       ---------|---------|----------|------------|----------|----------|-----------|----------
       LR_STOP  |         |  1       |            |          |          |           |
       ---------|---------|----------|------------|----------|----------|-----------|----------

    """

    # init state: workzones is clearted, queues for each direction are empty.
    # this state is not a part of states circle, it only indicates a start stage of DP.
    # should be never involve again.
    INIT = -1

    # vehicles left -> right wait at moment right -> left stopped but workzones not cleared.
    RL_STOP = LR_WAIT = 0

    # vehicles right -> left enter at moment left -> right stopped and workzones cleared.
    RL_ENTER = 1

    # vehicles right -> left wait at moment left -> right stopped but workzones not cleared.
    RL_WAIT = LR_STOP = 2

    # vehicles left -> right enter at moment right -> left stopped and workzones cleared.
    LR_ENTER = 3

    def __init__(self):
        """
        """
        self.current_state = States.INIT
        self.current_state_start_at = datetime.utcnow()

    def change_state(self):
        """Get next state by the given current_state.
        """

        self.current_state = (self.current_state + 1) % 4
        self.current_state_start_at = datetime.utcnow()
