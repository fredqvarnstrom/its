# -*- coding: utf-8 -*-
"""Location.
"""

from __future__ import division

import math
from config import *


class LocationBase(object):
    """
    """

    pass


class LocationPixel(LocationBase):
    """Location with pixel on screen.
    """

    def __str__(self):
        return "x(%s) y(%s)" % (self.x, self.y)

    def __init__(self):
        """
        """

        self._x = self._y = 0

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @x.setter
    def x(self, val):
        self._x = val

    @y.setter
    def y(self, val):
        self._y = val


class LocationLonLat(LocationBase):
    """Location with real lon / lat.

       When system run on real industry, we will need all locations in lon / lat
       and need do convertion between lon / lat and pixels.

       This class no use for now.
    """

    def __str__(self):
        return "lon(%s) lat(%s)" % (self.lon, self.lat)

    def __init__(self):
        """
        """

        self._lon = self._lat = 0

    @property
    def lon(self):
        return self._lon

    @property
    def lat(self):
        return self._lat

    @lon.setter
    def lon(self, val):
        self._lon = val

    @lat.setter
    def lat(self, val):
        self._lat = val


class Location(LocationPixel, LocationLonLat):
    """Location with both pixel and lon/lat.
    """

    def __init__(self):
        """
        """

        LocationPixel.__init__(self)
        LocationLonLat.__init__(self)

    @staticmethod
    def unit_length(pixel, meter):
        """Calculate unit length of meter -> pixel: how many pixels that represents 1 meter.
        """

        return pixel / meter

    @staticmethod
    def unit_x():
        """unit length at x direction (left <-> right)
        """

        return Location.unit_length(
            client_width, length_road_left + length_road_right + length_limitzones)

    @staticmethod
    def unit_y():
        """unit length at y direction (up <-> down), main road is at the middle of screen.
        """

        return Location.unit_length(
            client_height, 2 * length_side + width_road)

    @staticmethod
    def calculate_distance(loc1, loc2):
        """Calculate distance between 2 locations.

           Should be pixel based locations. return pixel distance as well.
        """

        return int(math.sqrt((loc1.x - loc2.x) ** 2 + (loc1.y - loc2.y) ** 2))
