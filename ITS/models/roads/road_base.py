# -*- coding: utf-8 -*-
"""Base class for road.
"""

from __future__ import division

from Queue import Queue

from models.location import Location
from models.direction import Direction

import config


class RoadBase(object):
    """Base logic for roads.
    """

    def __init__(self, name, length, direction):
        """
        """

        self.name = name
        self.length = length
        self.direction = direction
        self.vehicles_queue = Queue(length / config.length_vehicle)

        # define location of road end.
        self.unit_x = unit_x = Location.unit_x()
        self.unit_y = unit_y = Location.unit_y()

        self.location = Location()

        # define location of oppsite of limitzones
        self.location_oppsite = Location()

        # limit zones length (left <-> right) in pixel
        self.length_limitzones_pixel = config.length_limitzones * unit_x

        # limit zones width (up <-> down) in pixel
        self.width_limitzones_pixel = config.width_road * unit_y

        # sideway length (up <-> down) in pixel
        self.length_side_pixel = config.length_side * unit_y

        # sideway width (left <-> right) in pixel
        self.width_side_pixel = config.width_side * unit_x

    @property
    def Vehicles(self):
        """vehicles queue for the road.
        """
        return self.vehicles_queue

    def arrived_road_end(self, loc):
        """check if the given loc arrived the end of the road.
        """

        result = True
        offset_end = int(config.stop_offset * self.unit_x)

        direction = int(self.direction)

        if direction == Direction.TB and loc.y < self.location.y - 20:
            result = False
        elif direction == Direction.LR and loc.x < self.location.x - offset_end:
            result = False
        elif direction == Direction.RL and loc.x > self.location.x + offset_end - 20:
            result = False

        return result

    def get_total_enter_time(self):
        """calculate total vehicles' enter time at this road.
        """

        vehicles = list(self.vehicles_queue.queue)
        target_location = self.location

        total_time = sum([v.driving_time_to(target_location) for v in vehicles])

        return total_time

    def get_drivetime(self):
        """Calculate driving time for vehicles in this road to pass limitzones.
        """

        if self.vehicles_queue.empty():
            # vehicle queue is empty, no more drivetime needed.
            return 0

        else:
            # all vehicles in this road (within decision time window) finally enter and pass limitzones
            # means that the last vehicle enter and pass limitzones.
            last_vehicle = self.vehicles_queue.queue[-1]
            return last_vehicle.driving_time_to(self.location_oppsite)

    def get_drivertime_future(self, t):
        """Calculate driving time for vehicles in this road to pass limitzones in a future time.
        """

        return max(0, self.get_drivetime() - t)
