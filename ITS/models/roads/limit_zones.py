# -*- coding: utf-8 -*-
"""Class for limitzones (include workzones).
"""

from __future__ import division

from Queue import Queue

from models.location import Location
from models.direction import Direction
import config


class LimitZoneQueue(object):
    """Define a list based vehicles queue for limitzones that allow to pop from any position.
    """

    def __init__(self, maxsize):
        self.maxsize = maxsize
        self.queue = []

    def get(self, index=0):
        size = len(self.queue)
        return self.queue[index % size]

    def index(self, name):
        for (i, item) in enumerate(self.queue):
            if item.name == name:
                return i

    def pop(self, index=0):
        if 0 <= index < len(self.queue):
            del self.queue[index]

    def put(self, item):
        self.queue.append(item)

    def empty(self):
        return len(self.queue) == 0

    def full(self):
        return len(self.queue) >= self.maxsize

    def qsize(self):
        return len(self.queue)


class Limitzones(object):
    """Define limit zones that should be limited and controlled by traffic.

       Work zones are inside of limit zones.
    """

    def __init__(self, name):
        """Init limitzones.
        """

        self.name = name

        # as vehicles in limitzones need to be remove in any sequence, don't use queue.
        # create vehicles lists.
        # -- index 0: Direction.LR
        # -- index 1: Direction.RL
        self.vehicles_queues = [LimitZoneQueue(
            config.length_limitzones / config.length_vehicle) for i in range(2)]

        # create extra queues for leaving vehicles
        # -- need to allow vehicle continue driving after leave limitzones, for UI showing.
        self.leave_queue = Queue()

        # define limitzones location box, pixels.
        self.unit_x = Location.unit_x()
        self.unit_y = Location.unit_y()

        # mainroad width, pixel
        mainroad_width = config.width_road * self.unit_y

        # consider offset for signals position
        left_x = config.length_road_left * self.unit_x
        right_x = config.client_width - config.length_road_right * self.unit_x

        # top-left corner, pixels
        self.TL = Location()
        self.TL.x = left_x
        self.TL.y = (config.client_height - mainroad_width) * 0.5

        # top-right corner, pixels
        self.TR = Location()
        self.TR.x = right_x
        self.TR.y = self.TL.y

        # bottom-left corner, pixels
        self.BL = Location()
        self.BL.x = self.TL.x
        self.BL.y = self.TL.y + mainroad_width

        # bottom-right corner, pixels
        self.BR = Location()
        self.BR.x = self.TR.x
        self.BR.y = self.BL.y

        # limitzones width and height, pixels
        self.limitzones_width = self.TR.x - self.TL.x
        self.limitzones_height = self.BR.y - self.TR.y

        # workzones width and height, pixels
        self.workzones_width = config.length_workzones * self.unit_x
        self.workzones_height = config.width_workzones * self.unit_y

        # workzones top-left corner, pixels
        # assume workzones is at center-bottom of limitzones.
        self.workzones_TL = Location()
        self.workzones_TL.x = self.TL.x + 0.5 * (self.limitzones_width - self.workzones_width)
        self.workzones_TL.y = self.BL.y - self.workzones_height

        # we want to fully cover all sideways inside limitzones,
        # so first sideway starts from left of limitzones,
        # last sideway ends at right of limitzones.
        self.sideway_width = config.width_side * self.unit_x
        self.sideway_gap = (
            self.limitzones_width - self.sideway_width * config.count_sideroads
            ) / (config.count_sideroads - 1)

        self.offset_end = int(config.stop_offset * self.unit_x)

        # make sure sideway gap larger than 50 pixel so easy to be viewed.
        if self.sideway_gap < 50:
            raise ValueError("There is no enough gap between sideways.")

    def get_cleartime_one_way(self, vehicles_queue, target_location):
        """Calculate cleartime for limitzones for one way.
        """

        if vehicles_queue.qsize() == 0:
            # limitzones is empty, no more cleartime needed.
            return 0

        else:
            # cleartime is estimated by the last vehicle leaving time.
            ctime = max([v.driving_time_to(target_location) for v in vehicles_queue.queue])

            # now that queue not empty, don't allow ctime to be zero.
            return max(1.0, ctime)

    def get_cleartime(self):
        """Calculate total cleartime for limitzones.
        """

        cleartime = 0

        offset_end = self.offset_end

        if self.vehicles_queues[Direction.LR].qsize() > 0:
            # target location for left: bottom-right corner
            target_location = Location()
            target_location.x = self.BR.x + offset_end
            target_location.y = self.BR.y

            # get clear time for left
            cleartime = self.get_cleartime_one_way(
                self.vehicles_queues[Direction.LR], target_location)

        elif self.vehicles_queues[Direction.RL].qsize() > 0:
            # target location for right: top-left corner
            target_location = Location()
            target_location.x = self.TL.x - offset_end
            target_location.y = self.TL.y

            # get clear time for EW
            cleartime = self.get_cleartime_one_way(
                self.vehicles_queues[Direction.RL], target_location)

        return cleartime

    def get_cleartime_future(self, t):
        """Calculate total cleartime for limitzones in future.
        """

        return max(0, self.get_cleartime() - t)

    def inside_limitzones(self, vehicle):
        """Check if the given vehicle is within the limitzones.

           only used to check if a vehicle passed the limitzones (the vehicle already entered limitzones)
        """

        direction = int(vehicle.direction)
        location = vehicle.location

        left_x = int(self.TL.x)
        right_x = int(self.TR.x)

        if (
            (direction == Direction.LR and location.x > right_x) or
            (direction == Direction.RL and location.x < left_x)
        ):
            return False

        return True
