# -*- coding: utf-8 -*-
"""Class for side road.
"""

from __future__ import division
from road_base import RoadBase
import config


class RoadSide(RoadBase):
    """Sideway (side street).

       The only direction before sideday end is from top to bottom.
    """

    def __init__(self, name, sideway_index, length, direction):
        """
            -- sideway_index: the index of sideway, start from left.
        """

        super(RoadSide, self).__init__(name, length, direction)

        location = self.location
        location_oppsite = self.location_oppsite

        # from top to bottom, end location is at the crossroads between the sideway and limitzones.
        start_x = config.length_road_left * self.unit_x

        # we want to fully cover all sideways inside limitzones,
        # so first sideway starts from left of limitzones,
        # last sideway ends at right of limitzones.
        gap = (
            self.length_limitzones_pixel - self.width_side_pixel * config.count_sideroads
            ) / (config.count_sideroads - 1)

        location.x = location_oppsite.x = start_x + (gap + self.width_side_pixel) * sideway_index
        location.y = location_oppsite.y = self.length_side_pixel

        # vehicle should stop when arrives this location unless it is allowed to enter limitzones.
        self.offset_end = int(config.stop_offset * self.unit_y)

    def arrived_side_enter(self, loc):
        """check if vehicle arrived side street enter.
        """

        if self.location.x < loc.x < self.location.x + self.width_side_pixel:
            return True

        return False
