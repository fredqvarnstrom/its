# -*- coding: utf-8 -*-
"""Class for main road.
"""

from __future__ import division

from road_base import RoadBase

from models.direction import Direction

import config


class RoadMain(RoadBase):
    """Main road (driveway) left <-> right. Work zones are in this road.
    """

    def __init__(self, name, length, direction):
        """
        """

        super(RoadMain, self).__init__(name, length, direction)

        location = self.location
        location_oppsite = self.location_oppsite

        location.y = location_oppsite.y = self.length_side_pixel + 0.5 * self.width_limitzones_pixel

        # consider offset for signals position
        left_x = config.length_road_left * self.unit_x - 10
        right_x = config.client_width - config.length_road_right * self.unit_x

        direction = int(self.direction)

        if direction == Direction.LR:
            # from left to right, road end is at bottom-left corner of limitzones,
            # road oppsite end is at bottom-right corner of limitzones.
            location.x = left_x
            location_oppsite.x = right_x
            print "LR: location =>", location, "location_oppsite =>", location_oppsite

        elif direction == Direction.RL:
            # from right to left, road end is at top-right corner of limitzones,
            # road oppsite end is at top-left corner of limitzones.
            location.x = right_x
            location_oppsite.x = left_x
            print "RL: location =>", location, "location_oppsite =>", location_oppsite

        # vehicle should stop when arrives this location unless it is allowed to enter limitzones.
        self.offset_end = int(config.stop_offset * self.unit_x)

    def get_waiting_count(self):
        """get all waiting vehicles count in the road.
        """

        vs = list(self.Vehicles.queue)
        waitings = [v for v in vs if v.waiting]
        return len(waitings)

    def get_future_arrival_time(self, t=0):
        """get sum of arrival time that vehicles will arrive in a fixed time window (e.g. information horizon H).

           H: depends on maxmum vehicles queue capacity, e.g. length of road / length of vehicle.
           based on this assume, vehicles that will arrive in future time only include vehicles that already in
           vehicles queue but not change to waiting stage.
        """

        # all vehicles that alread in queue but not arrive yet.
        vs = list(self.Vehicles.queue)
        drivings = [v for v in vs if not v.waiting]

        # sum their arrival time -- the time for each vehicle that driving to end of the road.
        arrival_time = sum([v.driving_time_to(self.location) - t for v in drivings])

        return arrival_time

    def get_next_vehicle_arrival_time(self):
        """get next vehicle arrival time.

           if there is waiting vehicle on this road, return 0.
           if there is no coming vehicles on this road, return inf.
        """

        vs = list(self.Vehicles.queue)

        waitings = [v for v in vs if v.waiting]
        drivings = [v for v in vs if not v.waiting]

        if len(waitings) > 0:
            return 0

        if len(drivings) > 0:
            # arrives = [v.driving_time_to(self.location) for v in  drivings]
            # arrival_time = min(arrives)
            # print "v =>", drivings[0].location, self.location, arrival_time, arrives

            arrival_time = drivings[0].driving_time_to(self.location)
            # print "v =>", drivings[0].location, self.location, arrival_time

            return arrival_time

        else:
            return float('inf')
