# -*- coding: utf-8 -*-
"""Direction.
"""


class Direction(object):
    """
    """

    LR = 0   # from left to right
    RL = 1   # from right to left
    TB = 2   # from top to bottom
    BT = 3   # from bottom to top

    UNKNOWN = -1  # for some reason indicting error occurs

    @staticmethod
    def get_name(val):
        """Get readable direction names.
        """

        if Direction.LR == val:
            return "LR"

        if Direction.RL == val:
            return "RL"

        if Direction.TB == val:
            return "TB"

        if Direction.BT == val:
            return "BT"
