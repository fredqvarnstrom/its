Artificial Intelligence Transportation System


How to run alone
-----------------

This is main algorithm package, can be run for test alone without GUI. Results will be output to console only.

Steps to run:

1. make sure you have Python 2.7

2. cd to folder ITS, and type below command and a terminal shell:
   python main.py


How to use with GUI
--------------------

This algorithm package is designed to be embedded with any GUI software. An example GUI usage is provided in "gui/kivy" project.

Steps to work with GUI:

1. make sure ITS is in Python's search path. To do so, you can simply copy ITS folder to site-packages under Python's installation path, or add ITS's full path (something like /User/me/ITS) to sys.path at top of your GUI program.

2. check if ITS is in Python's search path, e.g. if your GUI software can visit ITS. cd your GUI path (something like /User/me/gui), start Python, type below command from Python's command shell:
    import ITS

    if ITS is not in Python's search path, you will get ITS cannot be found error.

3. in your GUI code, do below steps:
    -- import ITS

    -- call ITS.Generate_Elements(), that will return roads, limitzones, signals, client_width, client_hieght

    -- draw roads, limitzones, signals, as well as vehicles under your GUI. vehicles are included in roads or limitzones vehicles queues, depends on a vehicle is driving on roads or limitzones.

    -- call ITS.ITS_start() to start simulator (general dummy vehicles, update vehicles location etc).

    -- if need, call ITS.ITS_stop() to stop simulator.
