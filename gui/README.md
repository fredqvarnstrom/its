Artificial Intelligence Transportation System GUI Example


This is GUI package for demo ITS (Artificial Intelligence Transportation System). We provided kivy GUI, you can add other GUI as well.

To run ITS with kivy GUI, please follow below steps.


Install kivy
--------------

1. download kivy from https://kivy.org/#download, please select correct package to download according to your system.

2. follow instructions from https://kivy.org/docs/gettingstarted/installation.html to install kivy on your computer.


Run ITS with kivy
------------------

3. git clone our gui project to, example, /home/me/gui.

4. test if your kivy ready: we provided a simple "Hellow World" script to verify this. please cd your GUI folder (e.g. /home/me/gui) and type below command from terminal shell:
    kivy kivy/test/first.py

    You should see a black window that show "Hello Friend" at the center of screen.

    Please note, above kivy command is for Mac to start kivy program. If you use other system, please refer https://kivy.org/docs/guide/basic.html, go to "Running the application" section.

5. once step 4 test success, run ITS with kivy GUI by type below command from terminal shell:
    kivy kivy/main.py

    Again, this is for Mac example, please refer https://kivy.org/docs/guide/basic.html for how to run kivy from other system.

6. once kivy window show up with roads, signals, 2 buttons, click on "Start" button to start ITS simulator, click on "Stop" button to stop ITS simulator.

    Please besure you click "Stop" button before you close kivy window, that will notify ITS threads to stop.
