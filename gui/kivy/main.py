"""GUI for ITS with kivy.
"""


from kivy.app import App
from kivy.properties import ObjectProperty, Clock, BooleanProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.lang import Builder

from kivy.graphics import Color, Rectangle
from kivy.uix.button import Button
from kivy.uix.label import Label

from shapes.roads import Roads_String, RoadsWidget, CautionPopup
from shapes.signals import Signals_String, SignalsWidget
from shapes.vehicles import Vehicle_String, VehiclesPool

from kivy.core.window import Window

import os
import sys

root_dirs = os.path.dirname(os.path.realpath(__file__)).split(os.sep)[0:-2]
root_path = os.sep.join(root_dirs)
ITS_path = os.sep.join(root_dirs + ['ITS'])
ITS_path1 = os.sep.join(root_dirs + ['its_all', 'ITS'])

if root_path not in sys.path:
    sys.path.append(root_path)

if ITS_path not in sys.path:
    sys.path.append(ITS_path)

if ITS_path1 not in sys.path:
    sys.path.append(ITS_path1)

from ITS.main import ITSManage

import ITS.config

Builder.load_string(
    ''.join([
        Roads_String,
        Vehicle_String,
        Signals_String
    ])
    )


class ITSApp(App):
    """
    """

    caution_popup = ObjectProperty(None)
    inst_ITS = ObjectProperty(None)

    def build(self):
        """Main enter to start kivy.
        """

        # main container.
        root = FloatLayout()

        # register callbak for window resize event.
        root.bind(size=self._update_rect, pos=self._update_rect)

        self.caution_popup = CautionPopup()

        # draw a green background instead of default black background.
        with root.canvas.before:
            Color(0.41, 0.85, 0.36, 0.65)  # green; colors range from 0-1 not 0-255
            self.rect = Rectangle(size=root.size, pos=root.pos)

        # init ITS DP, get all drawing items.
        self.inst_ITS = ITSManage()
        self.inst_ITS.initialize()
        self.dp_roads = self.inst_ITS.roads
        self.dp_limitzones = self.inst_ITS.limitzones
        self.dp_signals = self.inst_ITS.signals
        dp_cx = self.inst_ITS.cx
        dp_cy = self.inst_ITS.cy

        # draw roads and limitzones (workzones inside of limitzones).
        root.add_widget(RoadsWidget(self.dp_roads, self.dp_limitzones, dp_cx, dp_cy))

        # draw vehicles on all roads and limitzones
        root.add_widget(VehiclesPool(self.dp_roads, self.dp_limitzones, dp_cx, dp_cy, root))

        # draw signals
        root.add_widget(SignalsWidget(self.dp_signals, dp_cx, dp_cy))

        # width of drawing area.
        cx = Window.size[0]
        cy = Window.size[1]

        # create a button for start DP
        self.btn_start = Button(
            size_hint=(None, None), text='Start',
            size=(150, 50), pos=(cx - 200, 60),
            on_press=self._start_dp)

        root.add_widget(self.btn_start)
        self.lb_status = Label(id='lb_status', text='Ready', font_size=25, pos=(cx/2 - 180, -cy/2 + 30))
        root.add_widget(self.lb_status)

        # init simulator and DP instance.
        self.dp_simulator = self.dp_client = None

        Clock.schedule_once(self._start_dp, 5)

        return root

    def _update_rect(self, instance, value):
        """Callback function for window resize event.

           Will be called whenever window resized.
        """

        # move/resize background area.
        self.rect.pos = instance.pos
        self.rect.size = instance.size

        # move buttons.
        self.btn_start.pos[0] = (Window.size[0] - 260)

    def _start_dp(self, *args):
        """Start ITS DP program.

           This function will actually start simulator and run DP at backend.
        """
        if self.btn_start.text == 'Start':
            if not self.inst_ITS.initialize_xbee():
                self.caution_popup.ids['lb_content'].text = 'Error\nFailed to initialize XBee Adapter.'
                self.caution_popup.open()
                # return False

        if self.btn_start.text == 'Stop':
            if self.dp_simulator and self.dp_client:
                self.inst_ITS.stop(self.dp_simulator, self.dp_client)
                self.dp_simulator = self.dp_client = None

                self.btn_start.text = 'Resume'
                self.lb_status.text = 'Paused'

        else:  # In case of 'Start' and 'Resume
            if not self.dp_simulator and not self.dp_client:
                self.b_running = True
                (self.dp_simulator, self.dp_client) = self.inst_ITS.start()
                self.btn_start.text = 'Stop'
                self.lb_status.text = 'Running...'


if __name__ == '__main__':
    ITSApp().run()
