# -*- coding: utf-8 -*-
"""Draw vehicles.
"""

from __future__ import division

from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ListProperty, \
    BooleanProperty, StringProperty

from kivy.core.window import Window
from kivy.clock import Clock

import os
import sys

root_dirs = os.path.dirname(os.path.realpath(__file__)).split(os.sep)[0:-3]
root_path = os.sep.join(root_dirs)
ITS_path = os.sep.join(root_dirs + ['ITS'])

if root_path not in sys.path:
    sys.path.append(root_path)

if ITS_path not in sys.path:
    sys.path.append(ITS_path)

from ITS.models.direction import Direction

# colors for vehicles
COLORS = [
    (1, 1, 1, .95),
    (0, 0.5, 0.5, .95),
    (0.5, 0.1, 0.5, .95),
    (0.5, 0.3, 0.3, .95),
    (0.5, 0.3, 0.8, .95),
]


Vehicle_String = '''
<VehicleWidget>:
    canvas:
        Color:
            rgba: self.vehicle_color

        RoundedRectangle:
            pos: self.vehicle_pos
            size: self.vehicle_size
'''


class VehicleWidget(Widget):
    close = BooleanProperty(False)
    vehicle_color = ListProperty([])
    vehicle_pos = ListProperty([])
    vehicle_size = ListProperty([])
    dt = NumericProperty(0)

    def __init__(self, dp_vehicle, dp_cx, dp_cy, **kwargs):
        """
        """

        self.dp_cx = dp_cx
        self.dp_cy = dp_cy
        self.dp_vehicle = dp_vehicle

        self._drawme()
        self.bind(size=self._update_size, pos=self._update_size)

        Clock.schedule_interval(self._update_pos, 1)

        super(VehicleWidget, self).__init__(**kwargs)

    def _map_location(self):
        """Convert dp location to kivy location.
        """

        # kivy cx, cy
        cx = Window.size[0]
        cy = Window.size[1]
        dp_cx = self.dp_cx
        dp_cy = self.dp_cy

        ratio_x = cx / dp_cx
        ratio_y = cy / dp_cy

        # map DP location to kivy location -- kivy's y start from bottom.
        pos_x = self.dp_vehicle.location.x * ratio_x
        pos_y = cy - self.dp_vehicle.location.y * ratio_y

        # adjust x, y according to direction to make sure vehicle driving on middle of road.
        adjust = 40

        if self.dp_vehicle.direction == Direction.LR:
            pos_y -= adjust
        elif self.dp_vehicle.direction == Direction.RL:
            pos_y += adjust
        elif self.dp_vehicle.direction == Direction.TB:
            pos_x += adjust

        # avoid go through workzones -- don't need in DP algorithm though.
        # workzone size and pos: hardcode just for demo.
        # workzone_size: (240.0, 120.0) workzone_pos: (680.0, 480.0)
        if self.dp_vehicle.direction == Direction.LR and 640 < pos_x < 680 + 260:
            pos_y += adjust * 1.3

        return pos_x, pos_y

    def _drawme(self):
        """Draw vehicle.
        """

        # # random color for vehicle
        # color_index = random.randint(0, 3)
        color_index = self.dp_vehicle.queue_index
        self.vehicle_color = COLORS[color_index]

        pos_x, pos_y = self._map_location()
        self.vehicle_pos = (pos_x, pos_y)

        if self.dp_vehicle.direction in [Direction.TB]:
            self.vehicle_size = (20, 25)
        else:
            self.vehicle_size = (25, 20)

    def _update_pos(self, dt):
        """
        """

        self.dt += dt
        pos_x, pos_y = self._map_location()
        self.vehicle_pos = (pos_x, pos_y)

        # reset vehicle's size as it may turn from TB to LR or RL
        if self.dp_vehicle.direction in [Direction.TB]:
            self.vehicle_size = (20, 25)
        else:
            self.vehicle_size = (25, 20)

    def _update_size(self, instance, value):
        """
        """

        self._drawme()


class VehiclesPool(Widget):
    """A set that holds all vehicles for drawing.
    """

    close = BooleanProperty(False)
    dt = NumericProperty(0)

    def __init__(self, roads, limitzones, dp_cx, dp_cy, root):
        """We need to convert vehicle's location from DP coordination to kivy coordination.

           -- dp_cy, dp_cy are client width and height from DP coordination.
        """

        self.drawing_pool = {}
        self.roads = roads
        self.limitzones = limitzones
        self.dp_cx = dp_cx
        self.dp_cy = dp_cy
        self.root = root

        Clock.schedule_interval(self._update_pool, 1)

        super(VehiclesPool, self).__init__()
        print(">>> VehiclesPool inited.")

    def _update_pool(self, dt):
        """
        """

        # define a local variable reference to avoid call self. in loop.
        root = self.root
        dp_cx = self.dp_cx
        dp_cy = self.dp_cy

        # pool for vehicles that are currently drawing on screen.
        drawing_pool = self.drawing_pool
        # pool for vehicles that are currently driving, no include those passed and left.
        driving_pool = set()

        self.dt += dt

        # all vehicle queues on all roads
        v_queues = [road.Vehicles for road in self.roads] + \
            self.limitzones.vehicles_queues + [self.limitzones.leave_queue]

        for v_queue in v_queues:
            vs = list(v_queue.queue)
            for v in vs:
                vid = id(v)
                driving_pool.add(vid)
                if vid not in drawing_pool:
                    vWidget = VehicleWidget(v, dp_cx, dp_cy)
                    root.add_widget(vWidget)
                    drawing_pool.update({vid: vWidget})

        # remove leaving vehicles if that driving outside of screen.
        for (vid, vWidget) in drawing_pool.items():
            if vid not in driving_pool:
                # remove from drawing pool
                root.remove_widget(vWidget)
