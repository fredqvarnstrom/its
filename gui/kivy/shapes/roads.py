# -*- coding: utf-8 -*-
"""Draw roads.
"""

from __future__ import division

from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from kivy.properties import OptionProperty, NumericProperty, ListProperty, \
        BooleanProperty, StringProperty

from kivy.core.window import Window
from kivy.clock import Clock

Roads_String = '''
<RoadsWidget>:
    canvas:
        Color:
            rgba: .55, .55, .55, root.alpha
        Rectangle:
            size: self.mainway_size
            pos: self.mainway_pos
        Color:
            rgba: .95, .6, .3, root.alpha
        Line:
            points: self.points_r1
            width: 2.0
            close: self.close

        Color:
            rgba: .55, .55, .95, root.alpha
        Rectangle:
            size: self.sideway_size
            pos: self.sideway_pos1
        Color:
            rgba: .55, .95, .55, root.alpha
        Rectangle:
            size: self.sideway_size
            pos: self.sideway_pos2
        Color:
            rgba: .95, .55, .55, root.alpha
        Rectangle:
            size: self.sideway_size
            pos: self.sideway_pos3

        Color:
            rgba: .95, .6, .3, root.alpha
        Line:
            width: 1.
            rectangle: self.rect_limit
            dash_length: 30
            dash_offset: 15
        Color:
            rgba: .95, .6, .3, 0.6
        Rectangle:
            size: self.workzone_size
            pos: self.workzone_pos

    Label:
        pos: root.workzone_label_pos
        text: 'Work Zones'
        color: root.label_color
    Label:
        pos: root.mainroad_label_pos
        text: 'Main road'
        color: root.label_color
    Label:
        pos: root.sideway_label_pos
        text: 'Side road'
        color: root.label_color
    Label:
        pos: (30, root.height * .92)
        text: 'North'
        color: root.label_color
    Label:
        pos: (root.width * .93, root.height * .59)
        text: 'East'
        color: root.label_color
    Label:
        pos: (130, root.height * .3)
        text: root.clearance_time_text
        color: root.label_color

<CautionPopup>:
    size_hint: .6, .5
    auto_dismiss: False
    title: 'CONSTRUCTRON'
    BoxLayout:
        orientation: 'vertical'
        padding: 20, 20
        spacing: 40
        Label:
            id: lb_content
            font_size: 35
            text: ""
        Label:
            font_size: 35
            text: ""
        Button:
            text: 'OK'
            on_press: root.dismiss()

'''


class CautionPopup(Popup):
    pass


class RoadsWidget(Widget):
    close = BooleanProperty(False)
    joint = OptionProperty('none', options=('round', 'miter', 'bevel', 'none'))
    cap = OptionProperty('none', options=('round', 'square', 'none'))
    dt = NumericProperty(0)

    alpha = NumericProperty(1.0)
    label_color = ListProperty([0.9, 0.9, 0.9, 0.4])

    # init roads vectors
    # -- r1: main road.
    # -- r2: side street 1, start from left.
    # -- r3: side street 2, start from left.
    # -- r4: side street 3, start from left.
    # -- rect_limit: limit zones.
    # -- rect_work: work zones.
    (
        points_r1, points_r2, points_r3, points_r4,
        rect_limit, rect_work
    ) = [ListProperty([]) for i in range(6)]

    # init signal labels.
    clearance_time_text = StringProperty('Clearance time: 0')

    def __init__(self, dp_roads, dp_limitzones, dp_cx, dp_cy):
        """Init location and size for all roads.
        """

        self.dp_roads = dp_roads
        self.dp_limitzones = dp_limitzones
        self.dp_cx = dp_cx
        self.dp_cy = dp_cy

        self._drawme()

        # register callbak for window resize event.
        self.bind(size=self._update_size, pos=self._update_size)

        # timer for update roads and limitzone data.
        Clock.schedule_interval(self._update_times, 1)

        super(RoadsWidget, self).__init__()

    def _drawme(self):
        """Draw roads.
        """

        cx = Window.size[0]
        cy = Window.size[1]

        dp_cx = self.dp_cx
        dp_cy = self.dp_cy
        dp_limitzones = self.dp_limitzones

        # kivy drawing width and height may be different with that from DP.
        # always do convertion.
        ratio_x = cx / dp_cx
        ratio_y = cy / dp_cy

        # note: kivy drawing rect start from bottom-left.

        # bottom left cornor for limit zones, converted to kivy pixels.
        BL_x = dp_limitzones.BL.x * ratio_x
        BL_y = cy - dp_limitzones.BL.y * ratio_y

        # limit zones width, height, mainway width, converted to kivy pixels.
        limitzones_width = dp_limitzones.limitzones_width * ratio_x
        self.mainway_width = limitzones_height = dp_limitzones.limitzones_height * ratio_y

        # mainway
        self.mainway_size = (cx, self.mainway_width)
        self.mainway_pos = (0, BL_y)
        self.mainroad_label_pos = (50, BL_y)

        # limitzone
        self.rect_limit = [
            BL_x, BL_y,
            limitzones_width, limitzones_height
        ]

        # workzone
        self.workzone_size = (
            dp_limitzones.workzones_width * ratio_x,
            dp_limitzones.workzones_height * ratio_y
            )
        self.workzone_pos = (dp_limitzones.workzones_TL.x * ratio_x, BL_y)
        self.workzone_label_pos = (self.workzone_pos[0] + 60, self.workzone_pos[1])

        print("workzone_size:", self.workzone_size, self.workzone_pos)

        # we want to fully cover all sideways inside limitzones,
        # so first sideway start from left of limitzones
        sideway_gap = dp_limitzones.sideway_gap * ratio_x
        self.sideway_width = dp_limitzones.sideway_width * ratio_x
        sideway_x1 = BL_x
        sideway_x2 = sideway_x1 + self.sideway_width + sideway_gap
        sideway_x3 = sideway_x2 + self.sideway_width + sideway_gap

        # mainway (main road) and sideway (side street) center y
        mainway_y = BL_y + 0.5 * self.mainway_width
        sideway_y = BL_y + self.mainway_width

        # sideways
        self.sideway_size = (self.sideway_width, sideway_y)
        self.sideway_pos1 = (sideway_x1, sideway_y)
        self.sideway_pos2 = (sideway_x2, sideway_y)
        self.sideway_pos3 = (sideway_x3, sideway_y)
        self.sideway_label_pos = (sideway_x1 + 20, sideway_y)

        self.points_r1 = [0, mainway_y, cx, mainway_y]

        # self.clearance_time_text = "Clearance time: 0"

    def _update_times(self, dt):
        """
        """

        self.dt += dt

        clearance_time = int(self.dp_limitzones.get_cleartime())
        self.clearance_time_text = "Clearance time: %d " % clearance_time

    def _update_size(self, instance, value):
        """
        """

        self._drawme()
