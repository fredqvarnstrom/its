# -*- coding: utf-8 -*-
"""Draw signals.
"""

from __future__ import division

from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ListProperty, \
    BooleanProperty, StringProperty

from kivy.core.window import Window
from kivy.clock import Clock

from datetime import datetime

Signals_String = '''
<SignalsWidget>:
    canvas:
        Color:
            rgba: self.LR_green_color
        Ellipse:
            size: self.signal_size
            pos: self.LR_green_pos
        Color:
            rgba: self.LR_yellow_color
        Ellipse:
            size: self.signal_size
            pos: self.LR_yellow_pos
        Color:
            rgba: self.LR_red_color
        Ellipse:
            size: self.signal_size
            pos: self.LR_red_pos

        Color:
            rgba: self.RL_green_color
        Ellipse:
            size: self.signal_size
            pos: self.RL_green_pos
        Color:
            rgba: self.RL_yellow_color
        Ellipse:
            size: self.signal_size
            pos: self.RL_yellow_pos
        Color:
            rgba: self.RL_red_color
        Ellipse:
            size: self.signal_size
            pos: self.RL_red_pos

    Label:
        pos: root.LR_label_pos
        text: root.LR_label_text
        color: root.label_color
    Label:
        pos: root.RL_label_pos
        text: root.RL_label_text
        color: root.label_color
'''


class SignalsWidget(Widget):
    close = BooleanProperty(False)
    dt = NumericProperty(0)

    # color for labels.
    label_color = ListProperty([0.9, 0.9, 0.9, 0.8])

    # drawing diameter of a signal.
    signal_size = ListProperty([])

    # alpha for signals enable and disable
    alpha_enable = 0.95
    alpha_disable = 0.15

    # default colors for signals.
    green_color = [0.36, 0.99, 0.39, alpha_disable]
    yellow_color = [0.99, 0.99, 0.39, alpha_disable]
    red_color = [0.99, 0.33, 0.39, alpha_disable]

    # init colors for all signals.
    LR_green_color = green_color
    RL_green_color = green_color

    LR_yellow_color = yellow_color
    RL_yellow_color = yellow_color

    LR_red_color = red_color
    RL_red_color = red_color

    # init pos for all signals and their labels.
    (
        LR_green_pos, LR_yellow_pos, LR_red_pos, LR_label_pos,
        RL_green_pos, RL_yellow_pos, RL_red_pos, RL_label_pos
    ) = [ListProperty([]) for i in range(8)]

    # init signal labels.
    LR_label_text = StringProperty('LR: 000 | 000')
    RL_label_text = StringProperty('RL: 000 | 000')

    def __init__(self, dp_signals, dp_cx, dp_cy, **kwargs):
        """init location and size for all signals.
        """

        self.dp_signals = dp_signals
        self.dp_cx = dp_cx
        self.dp_cy = dp_cy

        self._drawme()

        # register callbak for window resize event.
        self.bind(size=self._update_size, pos=self._update_size)

        # timer for update signal's color according to states.
        Clock.schedule_interval(self._update_color, 1)

        super(SignalsWidget, self).__init__(**kwargs)

    def _drawme(self):
        """Draw signals.
        """

        # drawing area width and height

        cx = Window.size[0]
        cy = Window.size[1]
        dp_cx = self.dp_cx
        dp_cy = self.dp_cy

        signal_LR = self.dp_signals[0]
        signal_RL = self.dp_signals[1]

        ratio_x = cx / dp_cx
        ratio_y = cy / dp_cy

        # size of a signal.
        self.signal_size = (30.0, 30.0)

        # left -> right signals and during / remaining label.
        LR_x = signal_LR.location.x * ratio_x - 30
        LR_y = cy - signal_LR.location.y * ratio_y + 100

        self.LR_green_pos = (LR_x, LR_y)
        self.LR_yellow_pos = (LR_x, LR_y - 40)
        self.LR_red_pos = (LR_x, LR_y - 80)
        self.LR_label_pos = (LR_x - 170, LR_y - 100)

        # right -> left signals and during / remaining label.
        RL_x = signal_RL.location.x * ratio_x
        RL_y = cy - signal_RL.location.y * ratio_y - 130

        self.RL_green_pos = (RL_x, RL_y)
        self.RL_yellow_pos = (RL_x, RL_y + 40)
        self.RL_red_pos = (RL_x, RL_y + 80)
        self.RL_label_pos = (RL_x + 100, RL_y + 40)

    def _update_color(self, dt):
        """
        """

        self.dt += dt

        signal_LR = self.dp_signals[0]
        signal_RL = self.dp_signals[1]

        during_LR = signal_LR.get_duration()
        during_RL = signal_RL.get_duration()

        # default for both end points is red.
        self.LR_green_color[3] = self.alpha_disable
        self.RL_green_color[3] = self.alpha_disable
        self.LR_yellow_color[3] = self.alpha_disable
        self.RL_yellow_color[3] = self.alpha_disable
        self.LR_red_color[3] = self.alpha_disable
        self.RL_red_color[3] = self.alpha_disable

        # update signal's enable / disable color according to DP signal's color.
        if signal_RL.color == 0:
            # RL enter
            self.RL_green_color[3] = self.alpha_enable
            self.LR_red_color[3] = self.alpha_enable
        elif signal_LR.color == 0:
            # LR enter
            self.LR_green_color[3] = self.alpha_enable
            self.RL_red_color[3] = self.alpha_enable
        elif signal_RL.color == 1:
            # RL wait
            if during_RL < 5:
                self.RL_yellow_color[3] = self.alpha_enable
            else:
                self.RL_red_color[3] = self.alpha_enable

            self.LR_red_color[3] = self.alpha_enable
        elif signal_LR.color == 1:
            # LR wait
            if during_LR < 5:
                self.LR_yellow_color[3] = self.alpha_enable
            else:
                self.LR_red_color[3] = self.alpha_enable

            self.RL_red_color[3] = self.alpha_enable
        else:
            # TODO: deal with yellow signal later.
            self.LR_red_color[3] = self.alpha_enable
            self.RL_red_color[3] = self.alpha_enable

        # update signals label
        during_LR_str = '%03d' % during_LR
        during_RL_str = '%03d' % during_RL

        # TODO: show second number later.
        self.LR_label_text = 'LR: %s | %s' % (during_LR_str, '000')
        self.RL_label_text = 'RL: %s | %s' % (during_RL_str, '000')

    def _update_size(self, instance, value):
        """
        """

        self._drawme()
